//soal 1 
var kataPertama = " saya ";
var kataKedua = " Senang ";
var kataKetiga = " belajar ";
var kataKeempat = " javascript ";
var upper = kataKeempat.toUpperCase();

console.log(kataPertama.concat(kataKedua, kataKetiga, upper))

//soal 2 
var kataPertama = Number("1");
var kataKedua = Number("2");
var kataKetiga = Number("4");
var kataKeempat = Number("5");

console.log(kataPertama+kataKedua+kataKetiga+kataKeempat)

//soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua= kalimat.substring(4,14); 
var kataKetiga= kalimat.substring(15,18); 
var kataKeempat= kalimat.substring(19,24); 
var kataKelima= kalimat.substring(25,31);  

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);

//soal 4
var nilai = 44;

if ( nilai >= 80) {
    console.log("Indeksnya A")
} else if ( nilai >=70 && nilai <80) {
    console.log("Indeksnya B")
} else if (nilai >=60 && nilai <70) {
    console.log("Indeksnya C")
} else if (nilai >=50 && nilai <60) {
    console.log("Indeksnya D")
} else  { (nilai <50)
 console.log("Indeksnya E")
}

//soal 5
var tanggal = 20;
var bulan = 6;
var tahun = 1999;

var stringTanggal=""
var stringBulan=""
var stringTahun= ""

switch(tanggal){
    case 1: stringTanggal="1"; break;
    case 2: stringTanggal="2"; break;
    case 3: stringTanggal="3"; break;
    case 4: stringTanggal="4"; break;
    case 5: stringTanggal="5"; break;
    case 6: stringTanggal="6"; break;
    case 7: stringTanggal="7"; break;
    case 8: stringTanggal="8"; break;
    case 9: stringTanggal="9"; break;
    case 10: stringTanggal="10"; break;
    case 11: stringTanggal="11"; break;
    case 12: stringTanggal="12"; break;
    case 13: stringTanggal="13"; break;
    case 14: stringTanggal="14"; break;
    case 15: stringTanggal="15"; break;
    case 16: stringTanggal="16"; break;
    case 17: stringTanggal="17"; break;
    case 18: stringTanggal="18"; break;
    case 19: stringTanggal="19"; break;
    case 20: stringTanggal="20"; break;
    case 21: stringTanggal="21"; break;
    case 22: stringTanggal="22"; break;
    case 23: stringTanggal="23"; break;
    case 24: stringTanggal="24"; break;
    case 25: stringTanggal="25"; break;
    case 26: stringTanggal="26"; break;
    case 27: stringTanggal="27"; break;
    case 28: stringTanggal="28"; break;
    case 29: stringTanggal="29"; break;
    case 30: stringTanggal="30"; break;
}

switch(bulan){
    case 1: stringBulan=" Januari "; break;
    case 2: stringBulan=" Februari "; break;
    case 3: stringBulan=" Maret "; break;
    case 4: stringBulan=" April "; break;
    case 5: stringBulan=" Mei "; break;
    case 6: stringBulan=" Juni "; break;
    case 7: stringBulan=" Juli "; break;
    case 8: stringBulan=" Agustus "; break;
    case 9: stringBulan=" September "; break;
    case 10: stringBulan=" Oktober "; break;
    case 11: stringBulan=" November "; break;
    case 12: stringBulan=" Desember "; break;
}

switch(tahun) {
    case 1991: stringTahun=" 1991 "; break;
    case 1992: stringTahun=" 1992 "; break;
    case 1993: stringTahun=" 1993 "; break;
    case 1994: stringTahun=" 1994 "; break;
    case 1995: stringTahun=" 1995 "; break;
    case 1996: stringTahun=" 1996 "; break;
    case 1997: stringTahun=" 1997 "; break;
    case 1998: stringTahun=" 1998 "; break;
    case 1999: stringTahun=" 1999 "; break;
    case 2000: stringTahun=" 2000 "; break;
}



console.log("Aku ulang tahun pada " + stringTanggal + stringBulan + stringTahun)